sumo (1.18.0+dfsg-4) unstable; urgency=medium

  * Team upload.

  [ Bas Couwenberg ]
  * Mark keyword-argument-repeated.patch as Applied-Upstream.

  [ Santiago Vila ]
  * Remove bashism from CMakeLists.txt. Closes: #1082135.
  * Update upstream metadata.

 -- Santiago Vila <sanvila@debian.org>  Mon, 28 Oct 2024 16:20:00 +0100

sumo (1.18.0+dfsg-3) unstable; urgency=medium

  * Team upload.
  * Disable Salsa CI, doesn't support large source packages.
  * Update keyword-argument-repeated.patch to also fix another script.

 -- Bas Couwenberg <sebastic@debian.org>  Mon, 24 Jul 2023 07:31:28 +0200

sumo (1.18.0+dfsg-2) unstable; urgency=medium

  * Team upload.
  * Add patch to fix 'SyntaxError: keyword argument repeated'.

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 23 Jul 2023 18:07:27 +0200

sumo (1.18.0+dfsg-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
    (closes: #1041637)
  * Export DEB_PYTHON_INSTALL_LAYOUT=deb_system instead of patching
    `setup.py install` to use --install-layout=deb.
  * Drop gtest-1.13.patch, included upstream. Refresh remaining patches.

 -- Bas Couwenberg <sebastic@debian.org>  Sat, 22 Jul 2023 16:52:08 +0200

sumo (1.17.0+dfsg-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Add patch to fix FTBFS with googletest 1.13.
    (closes: #1038897)
  * Update copyright file.
  * Refresh patches.
  * Exclude Windows devel file from repacked upstream release.
  * Bump Standards-Version to 4.6.2, no changes.
  * Add patch to not used deprecated distutils module.

 -- Bas Couwenberg <sebastic@debian.org>  Fri, 23 Jun 2023 12:10:37 +0200

sumo (1.15.0+dfsg-1) unstable; urgency=medium

  * Team upload.
  * Move from experimental to unstable.

 -- Bas Couwenberg <sebastic@debian.org>  Sat, 17 Dec 2022 09:20:08 +0100

sumo (1.15.0+dfsg-1~exp2) experimental; urgency=medium

  * Team upload.
  * Add gbp.conf to use pristine-tar & --source-only-changes by default.
  * Add patch to fix syntax issues with Python 3.

 -- Bas Couwenberg <sebastic@debian.org>  Thu, 15 Dec 2022 13:57:18 +0100

sumo (1.15.0+dfsg-1~exp1) experimental; urgency=medium

  * Team upload.
  * New upstream release.
  * Add patch to fix installation with setuptools 65.3.0.
    (closes: #1023520)
  * Reorder dependencies.
  * Replace python3 dependencies with ${python3:Depends} substvar.
  * Update watch file to handle common issues.
  * Bump Standards-Version to 4.6.1, no changes.
  * Update copyright file.
  * Add py3dist-overrides for sumolib & traci.
  * Add dh_python3 overrides for sumo-tools.
  * Use dh_python3 for shebang changes instead of patches.
  * Remove executable bit from Python files that aren't scripts.
  * Drop Homepage field from upstream metadata.
  * Remove extra license file
  * Update lintian overrides.
  * Check DEB_BUILD_OPTIONS for nocheck in dh_auto_test override
  * Add Rules-Requires-Root to control file.
  * Add Keywords to desktop file.
  * Remove spare manual page.
  * Fix jquery symlink in sumo-doc.
  * Refresh patches.
  * Move JNI libraries.

 -- Bas Couwenberg <sebastic@debian.org>  Wed, 14 Dec 2022 14:59:21 +0100

sumo (1.12.0+dfsg1-1) unstable; urgency=medium

  * [e174ad9] Fix typo
  * [0162713] New upstream version 1.12.0+dfsg1
  * [4554e20] Minor update of d-files
  * [ffcccc0] Set compat-level 13
  * [9dbf6ae] Avoid explicitly specifying -Wl,--as-needed linker flag.

 -- Anton Gladky <gladk@debian.org>  Fri, 11 Feb 2022 23:11:11 +0100

sumo (1.8.0+dfsg2-5) unstable; urgency=medium

  * [4a4b462] Add Breaks+Replaces: sumo (<< 1.8) for sumo-tools.
              (Closes: #980845)
  * [ee90f08] Allow reprotest to fail

 -- Anton Gladky <gladk@debian.org>  Sat, 23 Jan 2021 11:47:24 +0100

sumo (1.8.0+dfsg2-4) unstable; urgency=medium

  * [2d44042] Fix d/rules

 -- Anton Gladky <gladk@debian.org>  Fri, 22 Jan 2021 22:53:50 +0100

sumo (1.8.0+dfsg2-3) unstable; urgency=medium

  * [3c87058] No debug info for mipsel

 -- Anton Gladky <gladk@debian.org>  Thu, 21 Jan 2021 16:59:56 +0100

sumo (1.8.0+dfsg2-2) unstable; urgency=medium

  * [e62ac20] Do not run autotests in indep-mode
  * [6836a98] No debug info for mip* to avoid out of memory

 -- Anton Gladky <gladk@debian.org>  Wed, 20 Jan 2021 18:48:58 +0100

sumo (1.8.0+dfsg2-1) unstable; urgency=medium

  * [e563f46] New upstream version 1.8.0+dfsg2
  * [567c39a] Refresh patches
  * [16b858e] Update d/copyright
  * [c482b16] Remove d/clean
  * [7158927] Fix test
  * [6a5c16a] Update d/control
  * [71e2d68] Update d/rules
  * [f4f4e02] Add .gitlab-ci.yml
  * [a2fc178] Remove dh_make template from debian watch.
  * [2ca5f9b] Use secure URI in Homepage field.
  * [f890de3] Remove deprecated Encoding key from desktop file debian/sumo.desktop.
  * [fdd3cbe] Update standards version to 4.5.0, no changes needed.
  * [cbcd430] Set Standards-Version to 4.5.1

 -- Anton Gladky <gladk@debian.org>  Tue, 05 Jan 2021 20:48:37 +0100

sumo (1.4.0+dfsg1-1) unstable; urgency=medium

  * [cfb6793] New upstream version 1.4.0+dfsg1
  * [55b1f2f] Refresh patches
  * [9f7b1a6] Remove libxerces-c2-dev from build-depends
  * [262513e] Set upstream metadata fields:
              Bug-Submit, Repository, Repository-Browse.
  * [1a77a39] Remove python from depends in sumo-tools. (Closes: #948931)
  * [1826942] Suppress python-script-but-no-python-dep lintian warning
              (should be clarified with upstream)
  * [70c62c4] Exclude min.js files
  * [c025637] Exclude some more files from the tarball
  * [fad823b] Add +dfsg1 suffix
  * [1c5a8e7] Update d/copyright

 -- Anton Gladky <gladk@debian.org>  Thu, 16 Jan 2020 22:12:23 +0100

sumo (1.3.1-5) unstable; urgency=medium

  * [12e5965] Set SUMO_HOME to escape network usage during build.
              (Closes: #945343)
  * [33f60f2] Let autopkgtests be built without network
  * [fd3c58c] Set Standards-Version to 4.4.1
  * [89e5a7f] Set upstream metadata fields: Repository.

 -- Anton Gladky <gladk@debian.org>  Sat, 23 Nov 2019 20:02:54 +0100

sumo (1.3.1-4) unstable; urgency=medium

  * [a8daad4] Fix some lintian warnings/errors

 -- Anton Gladky <gladk@debian.org>  Wed, 02 Oct 2019 22:56:52 +0200

sumo (1.3.1-3) unstable; urgency=medium

  * [bea99fe] Use python3 instead of python2. (Closes: #938597)

 -- Anton Gladky <gladk@debian.org>  Mon, 30 Sep 2019 22:01:23 +0200

sumo (1.3.1-2) unstable; urgency=medium

  [ Anton Gladky ]
  * [4471ca1] Remove Build-Depends-Indep section

  [ Wookey ]
  * [b28a361] Clean docs after build. (Closes: #925273)
  * [eddc3af] Do not clean  webWizard index.html. (Closes: #925272)

 -- Anton Gladky <gladk@debian.org>  Mon, 30 Sep 2019 17:16:50 +0200

sumo (1.3.1-1) unstable; urgency=medium

  * [93410cf] Update d/copyright
  * [3768348] Update d/watch
  * [443a78d] Refresh patches
  * [bb26543] Minor update in d-files
  * [26cd9ee] Move test-file into the d/tests
  * [6b981ec] Update autopkgtests
  * [a444fea] Set compat-level to 12
  * [aaa2b7e] Set Standards-version to 4.4.0
  * [483bcb4] Remove obsolete fields Name from debian/upstream/metadata.
  * [38f14bd] Update d/upstream/metadata
  * [bf6aeec] New upstream version 1.3.1. (Closes: #931940)

 -- Anton Gladky <gladk@debian.org>  Mon, 30 Sep 2019 15:19:20 +0200

sumo (1.1.0+dfsg1-1) unstable; urgency=medium

  [ Michael Behrisch ]
  * [b063b03] Update d/copyright

  [ Anton Gladky ]
  * [a9773da] Update patches
  * [d520897] Use libxerces-c2-dev as build-depends
  * [88bcbf6] Update install files
  * [e0d8261] Use compat-level 11
  * [23d078f] Remove some files from the source
  * [2372382] Minor fixes
  * [cc81cb9] Trim trailing whitespace.
  * [c2b2305] Use secure copyright file specification URI.
  * [14aae51] Drop unnecessary dependency on dh-autoconf.
  * [f16a0e7] New upstream version 1.1.0+dfsg1

 -- Anton Gladky <gladk@debian.org>  Sun, 03 Feb 2019 10:17:52 +0100

sumo (0.32.0+dfsg1-2) unstable; urgency=medium

  * [6b65aa2] Fix FTBFS with GCC-8. (Closes: #897872)
  * [a1ad134] Update VCS-field
  * [005fdea] Set Standards-Version to 4.2.0

 -- Anton Gladky <gladk@debian.org>  Mon, 27 Aug 2018 12:28:23 +0200

sumo (0.32.0+dfsg1-1) unstable; urgency=medium

  * [74d6c2b] New upstream version 0.32.0+dfsg1
  * [e861a46] Refresh patches
  * [750e89d] Remove unneeded lines from d/rules
  * [f104b53] Set Standards-Version: 4.1.2. No changes
  * [0cd6e98] Remove testsuite-fiels in d/control

 -- Anton Gladky <gladk@debian.org>  Sun, 24 Dec 2017 18:07:43 +0100

sumo (0.30.0+dfsg1-1) unstable; urgency=medium

  * [78f2092] New upstream version 0.30.0+dfsg1
  * [07cc5b1] Refresh patches.
  * [c29b854] Bump Standards-Version: 4.0.0

 -- Anton Gladky <gladk@debian.org>  Sun, 23 Jul 2017 16:19:12 +0200

sumo (0.28.0+dfsg1-1) unstable; urgency=medium

  * [ca1e1eb] Exclude some bimary files.
  * [e4f942c] New upstream version 0.28.0+dfsg1
  * [66fa6c3] Refresh patches.
  * [58d6734] Apply cme fix dpkg.

 -- Anton Gladky <gladk@debian.org>  Tue, 08 Nov 2016 22:47:32 +0100

sumo (0.27.1+dfsg1-1) unstable; urgency=medium

  * [0622343] Imported Upstream version 0.27.1+dfsg1
  * [e550fac] Refresh patches.
  * [03913ad] Apply cme fix dpkg-copytight.

 -- Anton Gladky <gladk@debian.org>  Fri, 12 Aug 2016 20:34:54 +0200

sumo (0.26.0+dfsg1-1) unstable; urgency=medium

  * [08a309b] Refresh patches.
  * [e9c4564] Minor fixes in d/rules.
  * [5bdf3b9] Remove pycache-files.
  * [dbf9954] Set Standards-Version to 3.9.8. No changes.
  * [933fda6] Remove dpkg-dev from build-depends.
  * [4218818] Update d/copyright.
  * [d7c2d92] Update autopkgtest.
  * [a3f3e74] Fix unit-test
  * [98423f7] Imported Upstream version 0.26.0+dfsg1

 -- Anton Gladky <gladk@debian.org>  Sun, 15 May 2016 08:29:44 +0200

sumo (0.25.0+dfsg1-3) unstable; urgency=medium

  [ Anton Gladky ]
  * [584dfb1] Apply cme fix dpkg.
  * [80de93e] Update sumo-doc dependency.

  [ Santiago Vila ]
  * [cf767d8] Force Makefile check for errors. (Closes: #817033)
  * [5f3f351] Add python-matplotlib to the BD-Indep.

 -- Anton Gladky <gladk@debian.org>  Thu, 07 Apr 2016 23:35:27 +0200

sumo (0.25.0+dfsg1-2) unstable; urgency=medium

  * [e0c661b] Add marouter and netedit to usr/bin.
              Thanks to Jakob Erdmann <namdre.sumo@googlemail.com>

 -- Anton Gladky <gladk@debian.org>  Thu, 14 Jan 2016 19:30:02 +0100

sumo (0.25.0+dfsg1-1) unstable; urgency=medium

  * [c4a28a0] Imported Upstream version 0.25.0+dfsg1. (Closes: #802470)
  * [cf1cc2a] Refresh patches.
  * [f37c4fd] Refresh d/rules.
  * [566cd3b] Apply cme fix dpkg.
  * [8c9ea7f] Drop menu-file.
  * [05ad9d7] Add proper python-dependencies.
  * [21a0ed4] Update d/copyright.
  * [5e839de] Use system packaged OpenLayers.js.

 -- Anton Gladky <gladk@debian.org>  Tue, 12 Jan 2016 22:29:34 +0100

sumo (0.24.0+dfsg1-1) unstable; urgency=medium

  * [420b439] Update d/watch.
  * [f72c31b] Refresh patches.
  * [c21b7b2] Remove old scripts.
  * [227569e] Imported Upstream version 0.24.0+dfsg1

 -- Anton Gladky <gladk@debian.org>  Sat, 12 Sep 2015 21:38:47 +0200

sumo (0.23.0+dfsg1-2) unstable; urgency=medium

  * Move package from experimental to unstable

 -- Anton Gladky <gladk@debian.org>  Mon, 04 May 2015 21:26:14 +0200

sumo (0.23.0+dfsg1-2~exp1) experimental; urgency=medium

  * [de8037f] Drop msse2 buildfpags, unsupported by some platforms

 -- Anton Gladky <gladk@debian.org>  Mon, 20 Apr 2015 21:00:43 +0200

sumo (0.23.0+dfsg1-1~exp1) experimental; urgency=medium

  * [aad3ecb] Update d/copyright.
  * [f68572e] Imported Upstream version 0.23.0+dfsg1
  * [006ec0e] Refresh patches.
  * [d6fdb64] Apply cme fix for d/control.
  * [51222ad] Update d/copyright.

 -- Anton Gladky <gladk@debian.org>  Fri, 17 Apr 2015 17:38:22 +0200

sumo (0.22.0+dfsg-1~exp1) experimental; urgency=medium

  * [08bef65] Add Files-Excluded to d/copyright.
  * [01c89e5] Imported Upstream version 0.22.0+dfsg
  * [f73a708] Remove gl2ps from d/copyright.
  * [785774f] Refresh patches.
  * [b196497] Set Standards-Version: 3.9.6. No changes.
  * [923a4d9] Update homepage.
  * [34a84c3] Move d/upstream to d/upstream/metadata. Update it.
  * [b0c3f64] Fix lintian warnings in d/copyright.

 -- Anton Gladky <gladk@debian.org>  Thu, 20 Nov 2014 22:02:45 +0100

sumo (0.21.0+dfsg-1) unstable; urgency=medium

  * [d7097c6] Imported Upstream version 0.21.0+dfsg
  * [655607d] Refresh patches.
  * [04454ca] Add autopkgtest.

 -- Anton Gladky <gladk@debian.org>  Fri, 20 Jun 2014 23:03:11 +0200

sumo (0.20.0+dfsg-1) unstable; urgency=medium

  * [83c56d0] Remove debian/README.source
  * [fd5408c] Imported Upstream version 0.20.0+dfsg
  * [0c91c63] Refresh patches.

 -- Anton Gladky <gladk@debian.org>  Sat, 15 Mar 2014 16:50:07 +0100

sumo (0.19.0+dfsg-2) unstable; urgency=medium

  * [b7da9b1] Replace libxerces-c2-dev by libxerces-c-dev. (Closes: #732217)

 -- Anton Gladky <gladk@debian.org>  Sun, 15 Dec 2013 20:25:55 +0100

sumo (0.19.0+dfsg-1) unstable; urgency=low

  * [24595ef] Imported Upstream version 0.19.0+dfsg
  * [a2ba852] Refresh patch.
  * [0dc4baf] Set Standards-Version: 3.9.5. No changes.

 -- Anton Gladky <gladk@debian.org>  Sat, 30 Nov 2013 19:18:43 +0100

sumo (0.18.0+repack+dfsg-1) unstable; urgency=high

  * [4da44c5] Add script, which removes *.jar files. (Closes: #728777)
  * [7870538] Imported Upstream version 0.18.0+repack+dfsg
  * [401d451] Enable eulerspiral, as it was relicensed under LGPL.

 -- Anton Gladky <gladk@debian.org>  Mon, 18 Nov 2013 19:38:03 +0100

sumo (0.18~dfsg-1) unstable; urgency=low

  * [3a366bf] Use hardening=+all parameter on all archs except mips.
  * [d014241] Update homepage.
  * [23e6778] Imported Upstream version 0.18~dfsg
  * [5b43e89] Refresh patches.
  * [b27582b] Minor fix in debian/rules.
  * [6b5053e] Change section for sumo-tools package from "doc" to "science".
              (Closes: #717393)

 -- Anton Gladky <gladk@debian.org>  Fri, 30 Aug 2013 21:58:58 +0200

sumo (0.17.1~dfsg-6) unstable; urgency=low

  * [0f04b59] Use autotools-dev again on all platforms.

 -- Anton Gladky <gladk@debian.org>  Sun, 16 Jun 2013 01:00:29 +0200

sumo (0.17.1~dfsg-5) unstable; urgency=low

  * [bff2b7d] Do not use autoreconf on mips.

 -- Anton Gladky <gladk@debian.org>  Sat, 15 Jun 2013 09:35:16 +0200

sumo (0.17.1~dfsg-4) unstable; urgency=low

  * [3a52bc9] Add freeglut3-dev to BD to fix FTBFS on mips*.
  * [2d47a7a] Add keyword-entry into desktop-file.

 -- Anton Gladky <gladk@debian.org>  Thu, 13 Jun 2013 23:14:32 +0200

sumo (0.17.1~dfsg-3) unstable; urgency=low

  * [f7f8e0d] Use autoreconf.

 -- Anton Gladky <gladk@debian.org>  Wed, 12 Jun 2013 23:41:45 +0200

sumo (0.17.1~dfsg-2) unstable; urgency=low

  [ Daniel T Chen ]
  * [1ff46ca] Added missing libs to link, fixing FTBFS.

  [ Julian Taylor ]
  * [5961f4c] Fix FTBFS with ld --no-add-needed. (Closes: #710373)

  [ Anton Gladky ]
  * [ec4054e] Use canonical VCS-field.
  * [ec9218d] Update fix_scripts_headers.patch.
  * [431de9e] Do not install .gitignore file.
  * [c45ac64] Add autotools-dev to BD.
  * [891e775] Update fix_headers-patch.
  * [ff138d5] Use wrap-and-sort.

 -- Anton Gladky <gladk@debian.org>  Mon, 10 Jun 2013 22:37:01 +0200

sumo (0.17.1~dfsg-1) unstable; urgency=low

  * [1a2a17f] Imported Upstream version 0.17.1~dfsg
  * [1543af7] Remove obsolete DM-Upload-Allowed flag.
  * [f4521cb] Minor update in copyright file.
  * [74e45db] Bumper Standards-Version 3.9.4 (no changes).
  * [d29e6e2] Refresh patches.

 -- Anton Gladky <gladk@debian.org>  Wed, 08 May 2013 23:32:58 +0200

sumo (0.16.0~dfsg-1~exp1) experimental; urgency=low

  * [771d69c] Imported Upstream version 0.16.0~dfsg
  * [5da3835] Update/remove patches.
  * [caa9ffa] Rename netgensumo to netgenerate (upstream decision).

 -- Anton Gladky <gladky.anton@gmail.com>  Tue, 04 Dec 2012 18:53:26 +0100

sumo (0.15.0~dfsg-3) unstable; urgency=low

  * [6ae8190] Use compat level 9. Apply hardening of buildflags.
  * [daa56b5] Remove from compilation embedded copy of gl2ps,
              use packaged version instead.
  * [8c08de0] Add upstream file

 -- Anton Gladky <gladky.anton@gmail.com>  Wed, 18 Jul 2012 21:04:34 +0200

sumo (0.15.0~dfsg-2) unstable; urgency=low

  * [5813a09] Update fix_headers_of_scripts.patch.
  * [956cf12] Replace /usr/bin/netgen binary on /usr/bin/netgensumo.
              (Closes: #673931)

 -- Anton Gladky <gladky.anton@gmail.com>  Sun, 27 May 2012 16:28:54 +0200

sumo (0.15.0~dfsg-1) unstable; urgency=low

  * Initial packaging in Debian. (Closes: #669160)

 -- Anton Gladky <gladky.anton@gmail.com>  Mon, 23 Apr 2012 19:17:01 +0200

sumo (0.14.0-1) unstable; urgency=low

  * New upstream release.

 -- Michael Behrisch <behrisch@users.sourceforge.net>  Thu, 12 Jan 2012 23:46:43 +0100

sumo (0.13.1-1) unstable; urgency=low

  * New upstream release.

 -- Michael Behrisch <behrisch@users.sourceforge.net>  Tue, 01 Nov 2011 10:24:24 +0100

sumo (0.13.0-1) unstable; urgency=low

  * New upstream release.

 -- Michael Behrisch <behrisch@users.sourceforge.net>  Wed, 13 Jul 2011 16:58:57 +0200

sumo (0.12.3-1) unstable; urgency=low

  * New upstream release.

 -- Michael Behrisch <behrisch@users.sourceforge.net>  Wed, 13 Apr 2011 19:28:57 +0200

sumo (0.10.2-1) unstable; urgency=low

  * New upstream release.

 -- Michael Behrisch <behrisch@informatik.hu-berlin.de>  Mon, 16 Mar 2009 22:00:47 +0100

sumo (0.10.1-1) unstable; urgency=low

  * Update

 -- Michael Behrisch <behrisch@informatik.hu-berlin.de>  Sun, 15 Feb 2009 22:34:33 +0200

sumo (0.9.10-1) unstable; urgency=low

  * Initial Release

 -- Michael Behrisch <behrisch@informatik.hu-berlin.de>  Thu, 23 Oct 2008 23:04:33 +0200
